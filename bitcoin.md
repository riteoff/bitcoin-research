# Blockchain/Cryptocurrency Research
-------------------------------------------

## What is Blockchain?
- Software platform for digital assets
- List of cryptographically linked and secured records (a ledger)
- Each block contains a link to previous block
- Serves as public ledger for all bitcoin transactions
- All bitcoins can be verified on the blockchain?

## What is Cryptocurrency?
- A digital asset.
- Medium of exchange thus equivalent to a real monetary currency.
- Bitcoin was the first of such currencies.
- Secured using cryptography.
- Designed to be secure and anonymous?
- Hard/Impossible to counterfeit.
- Not fixed, price based on supply and demand.
- Created using data mining.

## What is mining?
- Bitcoin is not "printed" or "minted" like other currencies.
- Special software is used to solve math problems which result in a new block being added to the lockchain (data mining). This is computationally expensive, and requires a lot of hardware. However, the reward for mining bitcoin is currently 12.5 bitcoin worth over $50,000 dollars.
- It is the only way of adding new cryptocurrencies into circulation.

## What is Bitcoin (BTC)?
- Payment network.
- Digital currency.
- Founder unknown goes by pseudonym Satoshi Nakamoto.

### Pros
- decentralised so not governed by central banks
- lower transfer fees than wire transfers etc
- open-source so no central ownership
- "wallets" allow cold (offline) storage of bitcoins.
- Long term development means infrastructure is already there (mobile apps, desktop software etc.)

### Cons
- not private, all transactions visible and traceable
- governments seeking to legislate
- currently volatile, potential for growth = potential for decline
- Scalability - blockchain block size limited to 1MB creating a bottleneck of 3 transactions per second. Approaches to solving scalability issues:
	- Segregated Witness
	- Lightning Network

## Alt Coins?
Alternate forms of bitcoin or cryptocurrencies which in some cases fork (alternate development path) bitcoin. Not only forks of the development codebase but also forks of the blockchain thus sharing transaction histories up to the time and date of the fork. Noteworthy Forks include:

- Bitcoin Cash (BCH)
- Bitcoin Gold (BTG)

Other cryptocurrencies which are not based on bitcoin include:

- [Litecoin](https://litecoin.org/)
- [Monero](https://getmonero.org/)
- [Verge](https://vergecurrency.com/)
- [Dash](https://www.dash.org/)
- [Ethereum](https://www.ethereum.org/)
- [Ripple](https://ripple.com/xrp/)

[Here](https://medium.com/taipei-ethereum-meetup/pros-cons-of-the-top-10-cryptocurrencies-in-market-cap-everyone-need-to-know-3-monero-iota-and-854b66af9c89) is a good resource on pros and cons of top 10 cryptocurrencies.

### Pros
- Monery, Verge and Dash allow complete privacy, i.e. transactions can be obscured so that sender, receiver and amounts are hidden.
- More scalable and allows faster/cheaper blockchain transactions than Bitcoin???

### Cons
- Litecoin founder completely cashed out all of his coins which makes potential investors nervous.

### Issues
- The biggest disadvantage in my opinion is the obscurity of information despite the volume. There is no straightforward resource which succinctly describes how to use, invest, mine, develop bitcoin.

## Using Cryptocurrencies?
Fantastic steemit resource can be found [here](https://steemit.com/cryptocurrency/@cashbandicoot/so-you-want-to-trade-crypto-the-rookie-s-guide-to-buying-owning-and-selling-crypto-currencies-with-videos)

### How to Purchase?
- **CoinBase** seems to be the goto for purchasing bitcoins
### Storing Coins
- Recommended wallets include:
	- [Breadwallet](https://breadapp.com/)
	- [Blockchain Wallet](https://blockchain.info/wallet/#/) seems to be #1
### Getting alt-coins (Exchanges)?
- These can be used to purchase alt-coins using bitcoins (may require bidding)
	- [Bittrex](https://bittrex.com/)
	- [Poloniex](https://poloniex.com/)
### Selling?
Good resource on this [here](https://igaming.org/cryptocurrencies/section/how-to-sell-cryptocurrencies/)
- Safest way is using the exchange wallet. One must set the price for the coin and potential buyers will just purchase (I guess whether or not it sells depends on market demand).

## Current Trading Prices
As of the creation of this document 27/12/2017:

|Currency|Value per unit (USD)|
|---|---|
|Bitcoin|15832.29|
|Litecoin|273.49|
|Dash|1168.26|
|Bitcoin Cash|2791.9|
|Monero|429.40|
|Verge|0.1863|
|Ethereum|747.05|

## Predictions

* We can discuss this in person.